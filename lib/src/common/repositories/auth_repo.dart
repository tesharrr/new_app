import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthRepo{
  final FirebaseAuth auth = FirebaseAuth.instance;

  Future<void> signUp(String email, String password) async {
    await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
    );
  }

  Future<void> logIn(String email, String password) async {
    await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
    );
  }

  Future<void> logInGoogle() async {
    final GoogleSignInAccount? logInAccount = await GoogleSignIn().signIn();
    if (logInAccount != null) {
      final GoogleSignInAuthentication logInAuth = await logInAccount.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: logInAuth.accessToken,
        idToken: logInAuth.idToken,
      );
      await FirebaseAuth.instance.signInWithCredential(credential);
    }
  }

  Future<void> signInGoogle() async {
    final GoogleSignInAccount? logInAccount = await GoogleSignIn().signIn(); 
      final GoogleSignInAuthentication logInAuth = await logInAccount!.authentication;
      final credential = GoogleAuthProvider.credential(
        accessToken: logInAuth.accessToken,
        idToken: logInAuth.idToken,
      );
      await FirebaseAuth.instance.signInWithCredential(credential);
  }

  Future<void> otpCode(String code, String verificationId) async {
    PhoneAuthCredential credential = PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: code,
    );
    await FirebaseAuth.instance.signInWithCredential(credential);
  }

  Future<void> signOut() async {
    GoogleSignIn().signOut();
    FirebaseAuth.instance.signOut();
  }

}