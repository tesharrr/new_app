import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:new_app/src/common/bloc/bloc_bloc.dart';
import 'package:new_app/src/common/repositories/auth_repo.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
import '../../feauter/auth/page/view_pages.dart';
import '../../feauter/auth/widget/view_components.dart';
import '../../feauter/home/view_main_page.dart';
import '../style/view_style.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => AuthRepo(),
      child: BlocProvider(
        create: (context) => BlocBloc(repo: RepositoryProvider.of<AuthRepo>(context)),
        child: MaterialApp(
          scaffoldMessengerKey: Utils.messengerKey,
          theme: ThemeData(
            scaffoldBackgroundColor: AppColor.background,
            fontFamily: 'Nunito',
          ),
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          routes: {
            '/': (context) => AuthPage(),
            '/login': (context) => const LoginPage(),
            '/signup': (context) => const SignupPage(),
            '/verif_ph': (context) => const PhoneVerification(),
            '/forgot_pas': (context) => const ForgorPassword(),
            '/reset_pas': (context) => const ResetPassword(),
            '/main_page': (context) => const MainPage(),
            '/start': (context) => const StartPage(),
            //'/auth_page': (context) => AuthPage(),
          },
        ),
      ),
    );
  }
}
