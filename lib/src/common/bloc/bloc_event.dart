part of 'bloc_bloc.dart';

@immutable
sealed class BlocEvent {}

class SendDataEvent extends BlocEvent{
  final String email;
  final String password;

  SendDataEvent({
    required this.email, 
    required this.password
  });
}

class LogInEvent extends BlocEvent{
  final String email;
  final String password;

  LogInEvent({
    required this.email, 
    required this.password
  });
}

class GoogleAuthEvent extends BlocEvent{}

class SignOutEvent extends BlocEvent{}

class OtpCodeEvent extends BlocEvent{
  final String code;
  final String verificationId;

  OtpCodeEvent({
    required this.code, 
    required this.verificationId
  });
}