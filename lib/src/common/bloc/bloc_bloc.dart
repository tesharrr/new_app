import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:meta/meta.dart';
import 'package:new_app/src/common/repositories/auth_repo.dart';

import '../../feauter/auth/widget/view_components.dart';
part 'bloc_event.dart';
part 'bloc_state.dart';

class BlocBloc extends Bloc<BlocEvent, BlocState> {
  BlocBloc({required this.repo}) : super(RegistrationInitial()) {

    on<SendDataEvent>((event, emit) async {
      try{
        await repo.signUp(event.email, event.password);
        emit(RegistrationSucces());
      }on  FirebaseAuthException catch(e) {
        Utils.showSnackBar(e.message);
        emit(RegistrationError());
      }
    });

    on<LogInEvent>((event, emit) async {
      try{
        await repo.logIn(event.email, event.password);
        emit(RegistrationSucces());
      }on  FirebaseAuthException catch(e) {
        Utils.showSnackBar(e.message);
        emit(RegistrationError());
      }
    });

    on<GoogleAuthEvent>((event, emit) async {
      final GoogleSignInAccount? signInAccount = await GoogleSignIn().signIn();
      try{
        if (signInAccount != null){
          await repo.signInGoogle();
          emit(RegistrationSucces());
        }else{
          emit(RegistrationError());
        }
      }on  FirebaseAuthException catch(e) {
        Utils.showSnackBar(e.message);
        emit(RegistrationError());
      }
    });
  }

  final AuthRepo repo;
}
