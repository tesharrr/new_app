part of 'bloc_bloc.dart';

@immutable
sealed class BlocState{}

class RegistrationInitial extends BlocState{}
class RegistrationSucces extends BlocState{}
class RegistrationError extends BlocState{}

