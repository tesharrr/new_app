import 'package:flutter/material.dart';
import '../../common/style/view_style.dart';

class ShowDialogs{

  static void showDialogs(BuildContext context){
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return const Center(
          child: CircularProgressIndicator(
            color: AppColor.button2,
          ),
        );
      }
    );
  }

}