import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final user = FirebaseAuth.instance.currentUser!;

  void signUserOut() {
    try{
      GoogleSignIn().signOut();
      FirebaseAuth.instance.signOut();
      Navigator.of(context).pushNamed('/start');
    }catch(e){
      print(e);
      Navigator.pop(context);
    }
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed:(){
              signUserOut();
            }, //signUserOut,
            icon: Icon(Icons.logout),
          )
        ],
      ),
      body: Center(
        child: Text(
        'LOGED IN',// + user.email!,,
        style: TextStyle(
            fontSize: 28,
            color: Colors.white
          ),
      )),
    );
  }
}
