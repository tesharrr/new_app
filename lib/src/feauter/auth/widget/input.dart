import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../common/style/view_style.dart';

class InputWidget extends StatefulWidget {
  final String text;
  final TextEditingController? passwordController;
  final String? Function(String?)? validator;
  final AutovalidateMode? autovalidateMode;
  const InputWidget({
    super.key,
    required this.text,
    this.passwordController,
    this.validator,
    this.autovalidateMode,
  });

  @override
  State<InputWidget> createState() => _InputWidgetState();
}

class _InputWidgetState extends State<InputWidget> {
  bool isobscureText = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.passwordController,
      obscureText: isobscureText,
      style: TextStyle(color: AppColor.font2),
      decoration: InputDecoration(
        hintText: widget.text,
        hintStyle: TextStyle(color: AppColor.font2),
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColor.button1)),
        focusedBorder:
            OutlineInputBorder(borderSide: BorderSide(color: AppColor.font2)),
        suffixIcon: IconButton(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onPressed: () {
            setState(() {
              isobscureText = !isobscureText;
            });
          },
          icon: isobscureText
              ? SvgPicture.asset('assets/icon/eye-closed.svg')
              : SvgPicture.asset('assets/icon/eye.svg'),
        ),
      ),
      validator: widget.validator,
      autovalidateMode: widget.autovalidateMode,
    );
  }
}

class InputWidgetForm extends StatelessWidget {
  final TextEditingController? controller;
  final String text;
  final String? Function(String?)? validator;
  final AutovalidateMode? autovalidateMode;
  const InputWidgetForm({
    super.key, 
    this.controller, 
    required this.text, 
    this.autovalidateMode, 
    this.validator
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      style: TextStyle(color: AppColor.font2),
      decoration: InputDecoration(
        hintText: text,
        hintStyle: TextStyle(color: AppColor.font2),
        enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColor.button1)),
          focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColor.font2),
        ),
      ),
      validator: validator,
      autovalidateMode: autovalidateMode,
    );
  }
}
