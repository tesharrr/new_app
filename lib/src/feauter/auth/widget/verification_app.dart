import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:new_app/src/feauter/auth/page/code_page.dart';

import '../../../common/style/view_style.dart';

class LayoutVerif extends StatefulWidget {
  final String text1;
  final String route;
  const LayoutVerif({
    super.key, 
    required this.text1, 
    required this.route, 
  });

  @override
  State<LayoutVerif> createState() => _LayoutVerifState();
}

class _LayoutVerifState extends State<LayoutVerif> {
  String phone = '';

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
                child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraints.maxHeight),
              child: IntrinsicHeight(
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          widget.text1,
                          style: TextStyle(
                            color: AppColor.button2,
                            fontSize: 25,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'We will send you a text with a verification code',
                          style: TextStyle(
                            color: AppColor.font2,
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      TextFormField(
                        style: TextStyle(
                          color: AppColor.font1,
                          fontSize: 18,
                        ),
                        cursorColor: AppColor.font1,
                        decoration: InputDecoration(
                          hintText: '+7 000 000 00 00',
                          hintStyle: TextStyle(color: AppColor.font1),
                          filled: true,
                          fillColor: AppColor.font2,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(25),
                            borderSide: BorderSide(color: AppColor.button2),
                          ),
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 14.0),
                        ),
                        maxLength: 14,
                        keyboardType: TextInputType.phone,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp('[0-9+]+'))
                        ],
                        onChanged: (value) {
                          phone = value;
                        },
                        validator: (value) {
                          if (phone.length > 14 && phone.length < 10) {
                            return 'Enter your phone number';
                          }
                          return null;
                        },
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      const Spacer(),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: ElevatedButton(
                          onPressed: () async{
                            await FirebaseAuth.instance.verifyPhoneNumber(
                              verificationCompleted: (PhoneAuthCredential credential){},
                              verificationFailed: (FirebaseAuthException e){},
                              codeSent: (String verificationId, int? resendToken){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>CodePage(
                                  verificationId: verificationId, 
                                  phone: phone, 
                                  route: widget.route,
                                )));
                              },
                              codeAutoRetrievalTimeout: (String verification){},
                              phoneNumber: phone);
                          },
                          child: Text(
                            'Next',
                            style: AppFont.header2,
                          ),
                          style: ElevatedButton.styleFrom(
                            foregroundColor: AppColor.font1,
                            backgroundColor: AppColor.button2,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
            )
          )
        );
  }
}

class Verification {
  static AppBar numberVerif() {
    return AppBar(
      backgroundColor: AppColor.button2,
      foregroundColor: AppColor.font1,
      title: Text(
        'NECO',
        style: AppFont.header1,
      ),
      centerTitle: true,
    );
  }
}
