import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:new_app/src/feauter/auth/page/start_page.dart';
import '../home/view_main_page.dart';

class AuthPage extends StatelessWidget {
  final User? _user = FirebaseAuth.instance.currentUser;
  AuthPage({super.key});

  @override
  Widget build(BuildContext context) {
    if (_user != null) {
      return const MainPage();
    }else{
      return const StartPage();
    }
  }
}