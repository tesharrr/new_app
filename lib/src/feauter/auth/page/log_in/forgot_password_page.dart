import 'package:flutter/material.dart';
import '../../widget/view_components.dart';

class ForgorPassword extends StatefulWidget {
  const ForgorPassword({super.key});

  @override
  State<ForgorPassword> createState() => _ForgorPasswordState();
}

class _ForgorPasswordState extends State<ForgorPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Verification.numberVerif(),
      body: LayoutVerif(
        text1: 'Forgot your password?\nEnter your mobile number', 
        route: '/reset_pas',
      ),
    );
  }
}
