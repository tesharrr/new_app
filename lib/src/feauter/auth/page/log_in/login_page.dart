import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:new_app/src/common/bloc/bloc_bloc.dart';
import 'package:new_app/src/feauter/components/show_dialog.dart';
import '../../../../common/style/view_style.dart';
import '../../widget/view_components.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool toggle = false;
  bool isobscureText = true;
  final _formkey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  CircleAvatar emailButton(String rout) {
    return CircleAvatar(
      radius: 20,
      backgroundColor: AppColor.button2,
      child: BlocListener<BlocBloc, BlocState>(
        listener: (context, state) {
          if (state is RegistrationSucces) {
            Navigator.pushNamedAndRemoveUntil(
                context, '/main_page', (route) => false);
          }
        },
        child: IconButton(
          onPressed: () {
            BlocProvider.of<BlocBloc>(context).add(GoogleAuthEvent());
          },
          icon: Image.asset(rout),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: Padding(
                padding: EdgeInsets.all(25),
                child: Form(
                  key: _formkey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Spacer(),
                      SvgPicture.asset(
                        'assets/temp/logo.svg',
                        width: 100,
                      ),
                      const Spacer(),
                      Text(
                        'Log in',
                        style: TextStyle(
                          fontSize: 38,
                          fontWeight: FontWeight.w600,
                          color: AppColor.font2,
                        ),
                      ),
                      const Spacer(),
                      InputWidgetForm(
                          controller: _emailController, text: 'Email address'),
                      SizedBox(
                        height: 10,
                      ),
                      InputWidget(
                        text: 'Password',
                        passwordController: _passwordController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          height: 35,
                          child: TextButton.icon(
                            onPressed: () {
                              setState(() {
                                toggle = !toggle;
                              });
                            },
                            icon: toggle
                                ? SvgPicture.asset(
                                    'assets/icon/check-circle.svg')
                                : SvgPicture.asset(
                                    'assets/icon/check-circle-full.svg'),
                            label: Text(
                              'Remember me',
                              style: TextStyle(
                                fontSize: 14,
                              ),
                            ),
                            style: TextButton.styleFrom(
                              foregroundColor: AppColor.button2,
                            ),
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          height: 35,
                          child: TextButton.icon(
                            onPressed: () {
                              Navigator.of(context).pushNamed('/forgot_pas');
                            },
                            icon: SvgPicture.asset('assets/icon/question.svg'),
                            label: Text(
                              'Forgot your password',
                              style: TextStyle(
                                fontSize: 14,
                              ),
                            ),
                            style: TextButton.styleFrom(
                              foregroundColor: AppColor.button2,
                            ),
                          ),
                        ),
                      ),
                      const Spacer(),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: BlocListener<BlocBloc, BlocState>(
                          listener: (context, state) {
                            if (state is RegistrationSucces){
                              Navigator.pushNamedAndRemoveUntil(context, '/main_page', (route) => false);
                            }else if(state is RegistrationError){
                              Navigator.pop(context);
                            }
                          },
                          child: ElevatedButton(
                            onPressed: () {
                              ShowDialogs.showDialogs(context);
                              BlocProvider.of<BlocBloc>(context).add(
                                  LogInEvent(
                                      email: _emailController.text,
                                      password: _passwordController.text));
                            },
                            child: Text(
                              'Log in',
                              style: AppFont.header2,
                            ),
                            style: TextButton.styleFrom(
                              backgroundColor: AppColor.button2,
                              foregroundColor: AppColor.font2,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      OverflowBar(
                        overflowAlignment: OverflowBarAlignment.center,
                        children: [
                          Text(
                            'No account?',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              color: AppColor.font2,
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed('/signup');
                            },
                            child: Text(
                              'Sign up',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                                color: AppColor.button2,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'or connect with',
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColor.font2,
                        ),
                      ),
                      const Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          emailButton('assets/temp/google.png'),
                          emailButton('assets/temp/vk.png'),
                          emailButton('assets/temp/yandex.png'),
                        ],
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
