import 'package:flutter/material.dart';

import '../../../../common/style/view_style.dart';
import '../../widget/view_components.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({super.key});

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  bool isobscureText = true;
  bool isobscureText1 = true;
  final _passwordController = TextEditingController();
  final _formkey = GlobalKey<FormState>();

  Align resetAlign(String text, Color color, double fontSize ,FontWeight fontWeight){
    return Align(
      alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: TextStyle(
            color: color,
            fontSize: fontSize,
           fontWeight: fontWeight,
          ),
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Verification.numberVerif(),
      body: LayoutBuilder(
        builder: (context, constraints) => 
        SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Form(
                  key: _formkey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(height: 20,),
                      resetAlign('Create new password', AppColor.button2, 25, FontWeight.w500),
                      SizedBox(height: 10,),
                      resetAlign('Please enter and confirm your new password', AppColor.font2, 18, FontWeight.w400),
                      SizedBox(height: 30,),
                      resetAlign('New password', AppColor.button2, 20, FontWeight.w500),
                      SizedBox(height: 5,),
                      InputWidget(
                          text: 'Password', 
                          passwordController: _passwordController, 
                          validator: (value) => ValidApp().validatePassword(value),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      SizedBox(height: 20,),
                      resetAlign('Confirm new password', AppColor.button2, 20, FontWeight.w500),
                      //SizedBox(height: 5,),
                      InputWidget(
                          text: 'Confirm password', 
                          validator: (value) => ValidApp().resetPassword(value),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      const Spacer(),
                      SizedBox(height: 20,),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: ElevatedButton(
                          onPressed: (){
                            if(_formkey.currentState!.validate()){
                              Navigator.of(context).pushNamed('/login');
                            }
                          }, 
                          child: Text(
                            'Reset password',
                            style: AppFont.header2,
                          ),
                          style: ElevatedButton.styleFrom(
                            foregroundColor: AppColor.font2,
                            backgroundColor: AppColor.button2,
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                    ]
                  ),
                ),
              ) 
            ), 
          ),
        ) 
      ),
    );
  }
}