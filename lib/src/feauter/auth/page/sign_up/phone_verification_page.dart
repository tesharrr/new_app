import 'package:flutter/material.dart';
import '../../widget/view_components.dart';

class PhoneVerification extends StatefulWidget {
  const PhoneVerification({super.key});

  @override
  State<PhoneVerification> createState() => _PhoneVerificationState();
}

class _PhoneVerificationState extends State<PhoneVerification> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: Verification.numberVerif(),
      body: LayoutVerif(
        text1: 'Phone verification\nEnter your mobile number', 
        route: '/main_page', 
      ),
    );
  }
}
