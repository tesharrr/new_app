import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:new_app/src/common/bloc/bloc_bloc.dart';
import 'package:new_app/src/feauter/components/show_dialog.dart';
import '../../../../common/style/view_style.dart';
import '../../widget/view_components.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({super.key});

  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  bool toggle = false;
  bool isobscureText = true;
  bool isobscureText1 = true;
  RegExp emailRegax = RegExp(r'^[\w\.-]+@[\w-]+\.\w{2,3}(\.\w{2,3})?$');
  late bool isEmailValid;
  final _formkey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _nameController = TextEditingController();
  
  CircleAvatar emailButton(String rout) {
    return CircleAvatar(
      radius: 20,
      backgroundColor: AppColor.button2,
      child: BlocListener<BlocBloc, BlocState>(
        listener: (context, state) {
          if (state is RegistrationSucces){
            Navigator.pushNamedAndRemoveUntil(
            context, '/main_page', (route) => false);
          }
        },
        child: IconButton(
          onPressed: () {
            BlocProvider.of<BlocBloc>(context).add(GoogleAuthEvent());
          },
          icon: Image.asset(rout),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraints.maxHeight),
            child: IntrinsicHeight(
              child: Padding(
                padding: EdgeInsets.all(25),
                child: Form(
                  key: _formkey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Spacer(),
                      SvgPicture.asset(
                        'assets/temp/logo.svg',
                        width: 90,
                      ),
                      const Spacer(),
                      Text(
                        'Sign up',
                        style: TextStyle(
                          fontSize: 38,
                          fontWeight: FontWeight.w600,
                          color: AppColor.font2,
                        ),
                      ),
                      const Spacer(),
                      Text(
                        textAlign: TextAlign.center,
                        'By signing up, you agree to our',
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.w600,
                          color: AppColor.font2,
                        ),
                      ),
                      TextButton(
                          onPressed: () {},
                          child: Text(
                            'Term and privacy policy',
                            style: TextStyle(
                              height: 0.5,
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              color: AppColor.button2,
                            ),
                          )),
                      const Spacer(),
                      InputWidgetForm(
                        controller: _nameController,
                        text: 'User name',
                        validator: (value) =>
                            ValidApp().validateUsername(value),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InputWidgetForm(
                        controller: _emailController,
                        text: 'Email address',
                        validator: (value) =>
                            ValidApp().validateUsername(value),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InputWidget(
                        text: 'Password',
                        passwordController: _passwordController,
                        validator: (value) =>
                            ValidApp().validatePassword(value),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InputWidget(
                        text: 'Confirm password',
                        validator: (value) => ValidApp().resetPassword(value),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                      ),
                      const Spacer(),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: BlocListener<BlocBloc, BlocState>(
                          listener: (context, state) {
                            if (state is RegistrationSucces) {
                              Navigator.of(context).pushNamed('/verif_ph',).then((_) {
                                FirebaseAuth.instance.currentUser?.delete();
                               
                              });
                            }else if(state is RegistrationError){
                              Navigator.pop(context);
                            }
                          },
                          child: ElevatedButton(
                            onPressed: () {
                              ShowDialogs.showDialogs(context);
                              BlocProvider.of<BlocBloc>(context).add(
                                  SendDataEvent(
                                      email: _emailController.text,
                                      password: _passwordController.text));
                              //signUp();
                            },
                            child: Text(
                              'Sign up',
                              style: AppFont.header2,
                            ),
                            style: ElevatedButton.styleFrom(
                              foregroundColor: AppColor.font2,
                              backgroundColor: AppColor.button2,
                            ),
                          ),
                        ),
                      ),
                      const Spacer(),
                      Text(
                        'or connect with',
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColor.font2,
                        ),
                      ),
                      const Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          emailButton('assets/temp/google.png'),
                          emailButton('assets/temp/vk.png'),
                          emailButton('assets/temp/yandex.png'),
                        ],
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
